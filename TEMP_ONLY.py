
# coding: utf8


input = [
		'1040672',
		'1012007 / 1006022',
		'1039170 / 1039169 / 1012007 / 1006022',
		'1039170 / 1039169',
		'1026455 / 1012008 / 1012007 / 1006022'
		'1026455 / 1026352'
		]



def f(value):
    gtin, pid = value

    # liste a maintenir :
    valid_pids = ['1039169','1039170','1040672']
    # separateur de pid dans la valeur pid en entree ; initialement " / "
    pid_separator = " / "

    # debut du code, ne pas toucher ici
    current_pids = frozenset(sorted(pid.split(pid_separator)))
    valid_pids_set = frozenset(sorted(valid_pids))

    # verification de la presence d'au moins un element en commun
    if current_pids & valid_pids_set:
        return gtin


print f([1234567891011,'1039170 / 1039169 / 1012007 / 1006022'])

print int(round(-133 / 90.0))




null_value = None
null_test = "None"

print null_value, type(null_value)
null_value = str(null_value)
print null_value, type(null_value)
