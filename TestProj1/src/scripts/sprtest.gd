extends Node2D

export (Texture) var texture setget _set_texture
export var frameid = 0
var elapsed = 0 setget set_elapsed
var ress = []
onready var debugtext = get_node("DebugText")

func set_elapsed(value: int):
	return value

func _set_texture(value):
    # If the texture variable is modified externally,
    # this callback is called.
    texture = value #texture was changed
    update() # update the node


func _draw():
    draw_texture(texture, Vector2())


func _ready():

	for i in range(0, 26):
		var respath = "res://src/img/sg_anims/kick_%03d.png" % (i)
		print(respath)
		var res = load(respath)
		ress.append(res)

	var initial_res = ress[0]
	self._set_texture(initial_res)
	self._draw()
	# get_node("SpriteAnim").texture = res
	# var spr = get_node("SpriteAnim")


func _process(delta):

	elapsed += delta
	
	if elapsed > 0.02:
		elapsed = 0
		frameid = (frameid + 1 ) % 24
	
		self._set_texture(ress[frameid])
	
	debugtext.clear()
	debugtext.add_text("\n" + str(ress[frameid])+"\nFrame ID : " + str(frameid) + "\n")
	debugtext.add_text(str(1/delta))