# coding: utf8

import time
import json

recap_file = open("recap.txt", "w")

start_time = time.clock()
fname = "sgss.scon"

with open(fname) as config_file:
    config_data = json.load(config_file)

animations = config_data["entity"][0]["animation"]
vardefs = config_data["entity"][0]["var_defs"]

for anim in animations:
    print "\n\n%i - %s" % (anim["id"], anim["name"])

    if anim.get("eventline"):
        for e in anim["eventline"]:
            print "EVENTS : \t %s" % e
'''
    if anim.get("meta"):
    	if anim["meta"].get("valline"):
	        for m in anim["meta"]["valline"]:
	        	n = m[]
	        	
	            print "META :   \t %s" % m'''

print "\n\n"

print 26 % 24