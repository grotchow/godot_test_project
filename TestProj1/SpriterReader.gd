extends Node2D

const SPRITER_FILE = "res://src/img/SpriterTestForCoding/SpriterTestForCoding.scon"
var data = {}
var spriter_obj : Spriter


func _ready():
	data = load_scon()
	serialize_spriter_objects(data)


func load_scon():
	var data = {}
	var jsonfile = File.new()
	
	jsonfile.open(SPRITER_FILE, File.READ)
	data = parse_json(jsonfile.get_as_text())
	return data


func serialize_spriter_objects(data : Dictionary):
	
	
	pass


#
#
#
# instantiate classes
#
#
#


class Spriter:
	var folders : Array # of Folders
	var entities :  Array
	var tag_list : Array
	var atlas : Array


class SpriterFolder:
	extends SpriterElement
	var files : Array #of "Files"
	var atlasid : int


class SpriterFile:
	extends SpriterElement
	var type : int #SpriterFileType
	var pivot_x : float
	var pivot_y : float
	var width : int
	var height : int


class SpriterEntity:
	extends SpriterElement
	var spriter : Spriter
	var object_infos : Array
	var character_maps : Array
	var animations : Array
	var variables : Array #of SpriterVarDef


class SpriterObjectInfo:
	extends SpriterElement
	var object_type : int #SpriterObjectType
	var width : float
	var height : float
	var pivot_x : float
	var pivot_y : float
	var variables : Array #of SpriterVarDef


class SpriterAnimation:
	extends SpriterElement
	var entity : SpriterEntity
	var length : float
	var looping : bool
	var mainline_keys : Array # of SpriterMainlineKey
	var timelines : Array # of SpriterTimeline
	var eventlines : Array # of SpriterEventline
	var soundline : Array # of SpriterSoundline
	var meta : SpriterMeta


class SpriterMainlineKey:
	extends SpriterKey
	var bone_refs : Array # of SpriterRef
	var object_ref : Array # of SpriterObjectRef


class SpriterRef:
	extends SpriterElement
	var parent_id : int
	var timeline_id : int
	var key_id : int

	func _init():
		parent_id = -1


class SpriterObjectRef:
	extends SpriterRef
	var z_index : int


class SpriterTimeline:
	extends SpriterElement
	var object_type : int #SpriterObjectType
	var object_id : int
	var keys : Array # Of SpriterTimelineKey
	var meta : SpriterMeta

class SpriterTimelineKey:
	extends SpriterKey
	var spin : int
	var bone_info : SpriterSpatial
	var object_info : SpriterObject
	
	func _init():
		spin = 1


class SpriterSpatial:
	var x: float
	var y: float
	var angle: float
	var scale_x: float
	var scale_y: float
	var alpha: float

	func _init():
		scale_x = 1
		scale_y = 1
		alpha = 1


class SpriterObject:
	extends SpriterSpatial
	var animation_id : int
	var entity : int
	var folder: int
	var file: int
	var pivot_x: float
	var pivot_y: float
	var t: float


class SpriterCharacterMap:
	extends SpriterElement
	var maps : Array # of SpriterMapInstruction


class SpriterMapInstruction:
	var folder_id : int
	var file_id : int
	var target_folder_id : int
	var target_file_id : int

	func _init():
		target_folder_id = -1
		target_file_id = -1


class SpriterMeta:
	var varlines : Array # of SpriterVarline
	var tagline : SpriterTagline


class SpriterVarDef:
	extends SpriterElement
	var type : int #SpriterVarType
	var default_value : String
	var variable_value : SpriterVarValue


class SpriterVarline:
	extends SpriterElement
	var def : int
	var keys : Array #of SpriterVarlineKey


class SpriterVarlineKey:
	extends SpriterKey
	var value : String
	var variable_value : SpriterVarValue


class SpriterVarValue:
	var type : int #SpriterVarType
	var string_value : String
	var float_value : float
	var int_value : int


class SpriterEventline:
	extends SpriterElement
	var keys : Array # of SpriterKey


class SpriterTagline:
	var keys : Array # of SpriterTaglineKey


class SpriterTaglineKey:
	extends SpriterKey
	var tags : Array # Of SpriterTag


class SpriterTag:
	extends SpriterElement
	var tag_id : int


class SpriterSoundline:
	extends SpriterElement
	var keys : Array # of SpriterSoundlineKey


class SpriterSoundlineKey:
	extends SpriterKey
	var sound_object : SpriterSound


class SpriterSound:
	extends SpriterElement
	var folder_id : int
	var file_id : int
	var trigger : bool
	var panning : float
	var volume : float
	
	func _init():
		trigger = true
		volume = 1.0


class SpriterElement:
	var id : int
	var name : String


class SpriterKey:
	extends SpriterElement
	var time : float
	var curve_type : int #SpriterCurveType
	var c1 : float
	var c2 : float
	var c3 : float
	var c4 : float
	
	func _init():
		time = 0

		
# BELOW ARE enums
# problem is enum cannot be types : https://docs.godotengine.org/en/3.1/getting_started/scripting/gdscript/static_typing.html#cases-where-you-cant-specify-types
# so need to kill the ref in class def
# 
enum SpriterObjectType {SPRITE, BONE, BOX, POINT, SOUND, ENTITY, VARIABLE}
enum SpriterCurveType {LINEAR, INSTANT, QUADRATIC, CUBIC, QUARTIC, QUINTIC, BEZIER}
enum SpriterFileType {IMAGE, SOUND}
enum SpriterVarType {STRING, INT, FLOAT}