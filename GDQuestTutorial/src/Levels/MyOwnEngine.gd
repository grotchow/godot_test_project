extends Node

onready var txt : RichTextLabel = $Control/DebugStr
onready var cam : Camera2D = $Cam2D
var cam_zoom : Vector2 = Vector2.ONE
var timeloop : float = 0.0

func _ready():
    OS.window_fullscreen = true

func _process(delta:float) -> void:
    txt.set_text(String(timeloop))
    timeloop = sin(OS.get_ticks_msec()*0.0005 )
    cam_zoom = Vector2.ONE + Vector2.ONE* timeloop*0.1
    cam.rotate(timeloop)
    cam.zoom = cam_zoom