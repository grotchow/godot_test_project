extends KinematicBody2D
class_name MyBase2


export var max_speed : Vector2 = Vector2(300.0, 1000.0)
export var gravity : float = 3000.0
export var timescale : float = 1.0

var velocity : Vector2 = Vector2.ZERO

func _physics_process(delta) -> void:
    velocity.y += gravity * delta * timescale
    velocity.y = max(velocity.y, max_speed.y) #TO BE MOVED TO PLAYER SCRIPT
    #velocity = move_and_slide(velocity, max_slides=0)

    velocity = move_and_slide(velocity)
    velocity.x += (Input.get_action_strength("move_left") - Input.get_action_strength("move_right"))*800*delta

    # jump
    if Input.action_press("jump"):
        velocity.y = -4000 