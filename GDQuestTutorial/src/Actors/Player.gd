extends Actor

func _physics_process(delta: float) -> void:

    velocity.x += (Input.get_action_strength("move_left") - Input.get_action_strength("move_right"))*800*delta

    # jump
    if Input.action_press("jump"):
        velocity.y = -4000