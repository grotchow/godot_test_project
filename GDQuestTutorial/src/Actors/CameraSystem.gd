extends Camera2D

var main_actor : Transform2D
var zoom_level : float = 1.0


func _ready():
    main_actor = get_node("../BaseBody").transform
    print (main_actor)


# warning-ignore:unused_argument
func _process(delta : float) -> void:
    
    if Input.is_key_pressed(KEY_A) :
        zoom_level -=0.01
        self.zoom = Vector2.ONE * zoom_level
        print ( "zm_lvl = %s // zm = %s" % [zoom_level, self.zoom] )
    if Input.is_key_pressed(KEY_Z) :
        zoom_level +=0.01
        self.zoom = Vector2.ONE * zoom_level
        print ( "zm_lvl = %s // zm = %s" % [zoom_level, self.zoom] )
    self.position = main_actor.origin