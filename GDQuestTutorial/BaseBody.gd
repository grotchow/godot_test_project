extends Node2D

var spd : float = 0
var grav : float = 0
var pos : Vector2 = self.position
var deltatime : float
var move_angle : float
# GetSensors
onready var p : RayCast2D = $RayP
onready var r : RayCast2D = $RayR
onready var r_collided : bool = r.is_colliding()
onready var p_collided : bool = p.is_colliding()

func _ready() -> void:
    pass


func _physics_process(delta: float) -> void:
    deltatime = delta
    movex(spd, move_angle)


func _input(event: InputEvent) -> void:
    if Input.is_action_pressed("move_right"):
        spd += 200
        print ("input spd : ", String(spd))


func movex(disp_x : float = 0, angle : float = 0):
    # set RayFunction
    for i in range(0,5):
        r.transform.origin.x = 20.0
        r.transform.origin.y = -20.0 + 10*i
        r.set_cast_to(Vector2.RIGHT * disp_x)
        r.force_raycast_update()
        
        var collision : Object = r.get_collider()
        if collision:
            print ("i = %d" % (i))
            r.set_cast_to(Vector2.RIGHT * (to_local(r.get_collision_point()).x-20))
            print ("CollisionPoint = %s" % (Vector2.RIGHT * r.get_collision_point()).x)
            print ("CollisionNormal = %s" % r.get_collision_normal())
            break
    