extends KinematicBody2D
class_name MyBaseMovement


export var velocity : Vector2 = Vector2.ZERO
export var gravity : float = 450.0
export var max_speed : float = 200.0
export var jump_height : float = -200.0

#GET CHILD DATA
onready var bottom_ray : RayCast2D = $BottomRay

var initial_transform : Transform2D
var au_sol : bool

func _ready() -> void:
    initial_transform = self.transform


func get_jump_force(a : float, yt : int) -> float:
    # yt = y0 + v0*t + 0.5*a*t²
    var v0 : float
    # vt² = v0² + 2as
    # v0² = 2as
    #v0 = sqrt(2as)
    v0 = sqrt(-2 * yt * a ) *-1
    return v0


func _input(event: InputEvent) -> void:
    if event.is_action_pressed("jump"):
        velocity.y = get_jump_force(gravity, jump_height)
        au_sol = false
    if event.is_action_pressed("move_left"):
        velocity.x += -200
    if event.is_action_pressed("move_right"):
        velocity.x += 200
    if event.is_action_pressed("ui_down"):
        self.transform = initial_transform
        print(velocity.y)
    if event.is_action_pressed("ui_down"):
        self.rotate(PI / 4)

func _physics_process(delta) -> void:
    # velocity.x = Input.get_action_strength("move_left")*-5 + Input.get_action_strength("move_right")*5
    if au_sol and velocity.y > 0:
            velocity.y = 0
    else:
        velocity.y += gravity * delta
    
    #Y Movement
    var to_translate_y : float = velocity.y * delta
    bottom_ray.set_cast_to(Vector2(0,to_translate_y))
    bottom_ray.force_raycast_update()
    
    if bottom_ray.is_colliding():
        to_translate_y = to_local(bottom_ray.get_collision_point()).y
        au_sol = true
    
    translate(Vector2(0, to_translate_y))
    
      
    
    
    
    
func move_y(translation_amount_y : float ):
    pass    